OBJS=\
     aaac.o\
     commands.o

CC=gcc
CFLAGS= -O3 -fPIC

all:aaac.so aaac.h

aaac.so: $(OBJS)
	$(CC) $(CFLAGS) -shared -o aaac.so $(OBJS)

%.o:%.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm -rvf $(OBJS) aaac.so
