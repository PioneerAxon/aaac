/*
 * Copyright : Arth Patel, 2015
 *
 * This file is part of AAAC.
 *
 * AAAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AAAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AAAC.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <assert.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

#include "commands.h"

static char* command_strings [COM_MAX];
static int initialized = 0;

void
command_get_string (CommandType command, char* buff, int buff_size, const char* device_id, ...)
{
	assert (buff);
	va_list va;
	int x1, y1, x2, y2, duration;
	char* string;
	if (!initialized)
	{
		command_strings [COM_DeviceDetect] = " devices | tail -n +2";
		command_strings [COM_ScreenShot] = " shell screencap -p | sed 's/\\r$//g'";
		command_strings [COM_Touch] = " shell input tap "; // <x> <y>
		command_strings [COM_Swipe] = " shell input swipe "; // <x1> <y1> <x2> <y2> <duration in ms>
		command_strings [COM_Text] = " shell input text "; // <string>
		command_strings [COM_LongPress] = " shell input swipe "; // <x> <y> <x> <y> <duration in ms>
		initialized = 1;
	}
	assert (command > COM_NONE && command < COM_MAX);
	if (command != COM_DeviceDetect)
		assert (strlen (device_id) > 0);
	va_start(va, device_id);
	switch (command)
	{
		case COM_DeviceDetect:
			snprintf (buff, buff_size, "adb %s", command_strings [command]);
			break;
		case COM_ScreenShot:
			snprintf (buff, buff_size, "adb -s %s %s", device_id, command_strings [command]);
			break;
		case COM_Touch:
			x1 = va_arg (va, int);
			y1 = va_arg (va, int);
			snprintf (buff, buff_size, "adb -s %s %s %d %d", device_id, command_strings [command], x1, y1);
			break;
		case COM_Swipe:
			x1 = va_arg (va, int);
			y1 = va_arg (va, int);
			x2 = va_arg (va, int);
			y2 = va_arg (va, int);
			duration = va_arg (va, int);
			snprintf (buff, buff_size, "adb -s %s %s %d %d %d %d %d", device_id, command_strings [command], x1, y1, x2, y2, duration);
			break;
		case COM_Text:
			string = va_arg (va, char*);
			snprintf (buff, buff_size, "adb -s %s %s \'%s\'", device_id, command_strings [command], string);
			break;
		case COM_LongPress:
			x1 = va_arg (va, int);
			y1 = va_arg (va, int);
			duration = va_arg (va, int);
			snprintf (buff, buff_size, "adb -s %s %s %d %d %d %d %d", device_id, command_strings [command], x1, y1, x1, y1, duration);
			break;
		default:
			snprintf (buff, buff_size, "false");
	}
	va_end (va);
}
