/*
 * Copyright : Arth Patel, 2015
 *
 * This file is part of AAAC.
 *
 * AAAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AAAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AAAC.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __AAAC_H__
#define __AAAC_H__

typedef struct
{
	char id[256];
	char name[256];
} Device;

typedef struct
{
	Device *devices;
	int count;
} DeviceList;

DeviceList* aaac_get_device_list ();
void aaac_destroy_device_list (DeviceList* list);
void* aaac_get_screenshot (Device* dev, int* length);
void aaac_do_touch (Device* dev, int x, int y);
void aaac_do_swipe (Device* dev, int x1, int y1, int x2, int y2, int duration_ms);
void aaac_do_enter_text (Device* dev, const char* string);
void aaac_do_long_press (Device* dev, int x, int y, int duration_ms);

#endif
