/*
 * Copyright : Arth Patel, 2015
 *
 * This file is part of AAAC.
 *
 * AAAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AAAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AAAC.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __COMMANDS_H__
#define __COMMANDS_H__

typedef enum
{
	COM_NONE = 0,
	COM_DeviceDetect,
	COM_ScreenShot,
	COM_Touch,
	COM_Swipe,
	COM_Text,
	COM_LongPress,
	COM_MAX,
} CommandType;

void command_get_string (CommandType type,char* destination_buffer, int buffer_size, const char* device_id, ...);

#endif
