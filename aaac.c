/*
 * Copyright : Arth Patel, 2015
 *
 * This file is part of AAAC.
 *
 * AAAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AAAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AAAC.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "aaac.h"
#include "commands.h"

#define MALLOC_CHUNK 4096

static FILE*
get_process_pipe (char* command)
{
	const char redirection_string[] = " 2>&1";
	FILE* file;
	assert (command);
	char* full_command = (char*) malloc (strlen (command) + strlen (redirection_string) + 1);
	assert (full_command);
	strncpy (full_command, command, strlen (command) + 1);
	strcat (full_command, redirection_string);
	file = popen (full_command, "r");
	free (full_command);
	return file;
}

static char*
read_full_pipe (FILE* file, int* bytes_read)
{
	char* buffer = NULL;
	int buffer_size = 0;
	*bytes_read = 0;
	unsigned int br = 0;
	assert (file != NULL);
	while (!feof (file))
	{
		buffer = (char *) realloc (buffer, buffer_size + MALLOC_CHUNK);
		assert (buffer);
		buffer_size += MALLOC_CHUNK;
		br = fread (buffer + *bytes_read, 1, MALLOC_CHUNK, file);
		*bytes_read += br;
	}
	if (*bytes_read < buffer_size)
		buffer = (char *) realloc (buffer, *bytes_read);
	return buffer;
}

static int
close_process_pipe (FILE* file)
{
	assert (file != NULL);
	return pclose (file);
}

static char*
run_command (char* command, int* stream_len)
{
	FILE* file = get_process_pipe (command);
	char* stream;
	if (file == NULL)
		stream = NULL;
	else
	{
		stream = read_full_pipe (file, stream_len);
		close_process_pipe (file);
	}
	return stream;
}

DeviceList*
aaac_get_device_list ()
{
	DeviceList* list = (DeviceList*) malloc (sizeof (DeviceList));
	assert (list);
	list->count = 0;
	list->devices = NULL;
	int len = 0;
	char* stream;
	int consumed = 0;
	int total_consumed = 0;
	char command[4096];
	command_get_string (COM_DeviceDetect, command, 4096, "");
	stream = run_command (command, &len);
	while (1)
	{
		list->devices = (Device*) realloc (list->devices, sizeof (Device) * (list->count + 1));

		if (total_consumed >= len || sscanf (stream + total_consumed, "%255s %255s%n", list->devices [list->count].id, list->devices [list->count].name, &consumed) != 2)
		{
			break;
		}

		total_consumed += consumed;
		list->count++;
	}
	list->devices = (Device*) realloc (list->devices, sizeof (Device) * list->count);
	free (stream);
	return list;
}

void
aaac_destroy_device_list (DeviceList* list)
{
	if (!list)
		return;
	if (list->count > 0)
		free (list->devices);
	free (list);
}

void*
aaac_get_screenshot (Device* dev, int* length)
{
	assert (dev);
	char command [4096];
	command_get_string (COM_ScreenShot, command, 4096, dev->id);
	return run_command (command, length);
}

void
aaac_do_touch (Device* dev, int x, int y)
{
	assert (dev);
	int dummy;
	char* dummy_str;
	char command [4096];
	command_get_string (COM_Touch, command, 4096, dev->id, x, y);
	dummy_str = run_command (command, &dummy);
	if (dummy != 0)
		free (dummy_str);
}

void
aaac_do_swipe (Device* dev, int x1, int y1, int x2, int y2, int duration_ms)
{
	assert (dev);
	int dummy;
	char* dummy_str;
	char command [4096];
	command_get_string (COM_Swipe, command, 4096, dev->id, x1, y1, x2, y2, duration_ms);
	dummy_str = run_command (command, &dummy);
	if (dummy != 0)
		free (dummy_str);
}

void
aaac_do_enter_text (Device* dev, const char* string)
{
	assert (dev);
	int dummy;
	char* dummy_str;
	char command [4096];
	command_get_string (COM_Text, command, 4096, dev->id, string);
	dummy_str = run_command (command, &dummy);
	if (dummy != 0)
		free (dummy_str);
}

void
aaac_do_long_press (Device* dev, int x, int y, int duration_ms)
{
	assert (dev);
	int dummy;
	char* dummy_str;
	char command [4096];
	command_get_string (COM_LongPress, command, 4096, dev->id, x, y, duration_ms);
	dummy_str = run_command (command, &dummy);
	if (dummy != 0)
		free (dummy_str);
}
